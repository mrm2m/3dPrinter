$fn=100;

include <arc.scad>

difference() {
    union() {
        //Ausenwand Zylinder
        cylinder(d=64, h=50, center=true);
    }
    union() {

        // Innenwand Zylinder
        cylinder(d=59, h=70, center=true);

        // endanschläge
        for(i=[0:3]) {
            rotate([0,0,i*90]) {
                translate([27.5,0,26.5]) {
                    cube([10,7,18], center=true);
                }
                translate([27,0,17.5]) {
                    rotate([0,90,0]) {
                        cylinder(d=7, h=10, center=true);
                    }
                }
            }
            rotate([0,0,90*i+10]) {
                translate([0,55/2,17.5]) {
                    rotate([90,0,0]) {
                        cylinder(d=7, h=10, center=true);
                    }
                }
                translate([0,0,14]) {
                    arc(10, 7, 35, 10);
                }
            }
        }
    }
}
