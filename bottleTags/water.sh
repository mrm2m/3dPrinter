#!/bin/bash

if [[ -z $1 ]]; then
	echo -e "\nUsage: ./water.sh nickname [2ndNickName] [nthNickName]\n"
	exit 1
fi

if [[ ! $(which openscad) ]]; then
	echo "Please install openscad!"
fi

for i in "$@"; do
	echo $i
	sed "s/NickName/$i/" waterNames.scad > tmp.scad
	openscad -o toPrint/water_${i}.stl tmp.scad
	rm tmp.scad
done
