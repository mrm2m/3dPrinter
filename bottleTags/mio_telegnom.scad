$fn = 100;
include <Write.scad>

module name_tag () {
	difference() {
		union () {
			cylinder(r1=21.5, r2=15, h=26);
			rotate([0, 0, -45]) writecylinder(name, [0,0,0], 17, 26, h=8, t=20, font="orbitron.dxf");
		}
		cylinder(r1=19.5, r2=13, h=26.1);
		cylinder(r=25.5, h=1, center=true);
		difference () {
			cylinder(r=80, h=26);
			cylinder(r1=22, r2=15.5, h=26.1);
			cylinder(r=20, h=1, center=true);
		}
		translate([0,0,-1]) cube([50,50,50]);
	}
}

rotate([0,0,45])
translate([0,  0, 0]) name_tag(name="telegnom");
