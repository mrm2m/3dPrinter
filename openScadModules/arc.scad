module arc(h, t, r, grad) {
    //renders an arc
    render() {
        difference() {
            rotate_extrude($fn = 100)
                translate([r - h, 0, 0])
                    square([h,t]);

            translate([0,-(r+1),-.5])
                cube ([r+1,(r+1)*2,t+1]);

            rotate([0,0,180-grad])
            translate([0,-(r+1),-.5])
                cube ([r+1,(r+1)*2,t+1]);

        }
    }
}
