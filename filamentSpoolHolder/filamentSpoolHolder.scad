$fn=200;

difference() {
    cylinder(d1=70, d2=25, h=30, center=true);
    union() {
        for (i=[-1,1]) {
            translate([0,0,i*13]) {
                cylinder(d=19, h=10, center=true);
            }
        }
        cylinder(d=7, h=44, center=true);
        for (i=[0:3]) {
            rotate([0,0,90*i]) {
                translate([25,25,0]) {
                    cylinder(d=35, h=37, center=true);
                }
            }
        }
    }
}